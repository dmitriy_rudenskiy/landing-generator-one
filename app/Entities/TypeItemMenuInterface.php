<?php

namespace App\Entities;

interface TypeItemMenuInterface
{
    /**
     *
     */
    const TYPE_LOGO = 1;

    /**
     *
     */
    const TYPE_ITEM = 3;

    /**
     *
     */
    const TYPE_PHONE = 5;
}